$('.ui.search')
  .search({
    type          : 'category',
    minCharacters : 3,
    debug: true,
    verbose: true,
    error : {
      source      : 'Cannot search. No source used, and Semantic API module was not included',
      noResults   : 'Your search returned no results in DOAJ. <a href="mailto:bora@uib.no">If you believe there has been an error, please contact us</a>. <br><br>If your application is for an Open Access article in a subscription journal, <a href="#hybrid">click here</a>.',
      logging     : 'Error in debug logging, exiting.',
      noTemplate  : 'A valid template name was not specified.',
      serverError : 'There was an issue with querying the server.',
      maxResults  : 'Results must be an array to use maxResults setting',
      method      : 'The method you called is not defined.'
    },
    apiSettings   : {
      onResponse: function(doajResponse) {
        var
          response = {
            results : {}
          }
        ;
        // translate GitHub API response to work with search
        $.each(doajResponse.results, function(index, item) {
          var
            publisher   = item.bibjson.country || 'Unknown',
            maxResults = 20
          ;
          if(index >= maxResults) {
            return false;
          }

          // create new language category
          if(response.results[publisher] === undefined) {
            response.results[publisher] = {
              name    : publisher,
              results : []
            };
          }

          // get apc information
          if(item.bibjson.apc !== undefined) {
            var price    = item.bibjson.apc.average_price,
                currency = item.bibjson.apc.currency
              ;
          }
          else{
            var price    = 'Unknown',
                currency = 'APC'
              ;
          }

          // get license information
          if(item.bibjson.license !== undefined) {
            var open_access = item.bibjson.license[0].open_access,
                license     = item.bibjson.license[0].type
              ;
          }
          else{
            var open_access    = 'Unknown',
                license = 'Unknown license'
              ;
          }

          // get issn, electronic and print
          var eissn = '',
              pissn = '';
          $.each(item.bibjson.identifier, function(index, id) {
            if(id.type = 'eissn') {
              eissn = id.id;
            };
            if(id.type = 'eissn') {
              pissn = id.id;
            };
          });
        
          // add result to category
          response.results[publisher].results.push({
            id          : item.id,
            title       : item.bibjson.title,
            eissn       : eissn,
            pissn       : pissn,
            description : item.bibjson.publisher, // + '<br><i>' + item.bibjson.keywords + '</i>',
            publisher   : item.bibjson.publisher,
            keywords    : item.bibjson.keywords,
            price       : price + ' ' + currency,
            doaj_price  : price,
            currency    : currency,
            title_url   : item.bibjson.link[0].url,
            country     : item.bibjson.country,
            oa_status   : 0,
            doaj        : 1,
            open_access : open_access,
            license     : license
          });
        });
        return response;
      },
      url: 'https://doaj.org/api/v1/search/journals/{query}'
    },
    onResponse : function() {

    },
    onSelect: function(result, response) {
        var skjemaker = config.form,
            country   = result.country.toLowerCase(),
            html      = ''
        ;

        html += '<div class="ui stacked padded segment journal">';

        if(result.oa_status == 0) {
          html += ''
            + '<button class="ui green huge button">Open Acess</button>' 
          ;
        }

        if(result.license.indexOf("CC") >= 0) { 
          html += ''
            + '<button class="ui huge basic button"><i class="creative commons icon"></i>' + result.license + '</button>'
          ;
        }
        else {
          html += ''
            + '<button class="ui huge basic button"><i class="certificate icon"></i>' + result.license + '</button>'
          ;
        }

        /*if(result.price == 'Ukjent APC') {
          html += ''
            + '<button class="ui green basic huge button">Ukjent APC</button>'
          ;
        }
        else {
          html += ''
            + '<button class="ui green basic huge button">100% av ' + result.price + ' dekkes</button>'
          ;
        }*/

        html += ''
          + '<h1 class="ui header"><span class="flag-icon flag-icon-' + country + '"></span></i> <a target="_blank" href="' + result.title_url + '">' + result.title + ' <i class="external icon"></i></a><div class="sub header">' + result.publisher + '</div></h1>'
        ;

        if(result.eissn != result.pissn) { 
          html += ''
            + '<p>' + result.eissn + ' (electronic issn) / ' + result.pissn + ' (print issn)</p>'
          ;
        }
        else {
          html += ''
            + '<p>' + result.eissn + ' (electronic issn)</p>'
          ;
        }
  
        if(result.keywords !== undefined) {
          html += ''
            + '<div class="journal-keywords">'
          ;
        }
        $.each(result.keywords, function(index, keyword) {
          html += ''
            + '<a class="ui tag label">' + keyword + '</a>'
          ;
        });
        if(result.keywords !== undefined) {
          html += ''
            + '</div>'
          ;
        }

/*        html += ''
          + '<div class="apc">100% av den estimerte kostnaden på ' + result.price + ' dekkes <i class="asterisk red icon"></i></div>'
          + '<div class="apc caveat"><i class="asterisk red icon"></i> du må selv sjekke og registrere den summen du har fått oppgitt fra forlaget</div>'
        ;*/

/*        html += ''
          + '<i class="chevron down huge icon"></i>'
          + '<div class="ui horizontal divider">Sjekk DBH nivå på tidsskriftet</div>'  
          + '<form class="ui equal width form">'
          +      '<div class="field">'
          +        '<div class="ui checkbox">'
          +        '<a class="ui red basic fluid button" target="_blank" href="https://dbh.nsd.uib.no/publiseringskanaler/KanalTreffliste.action?enkeltSok=' + result.title + '&__checkbox_bibsys=true&sok;.avansert=false&treffliste;.tidsskriftTreffside=1&treffliste;.forlagTreffside=1&treffliste;.vis=true">' + 'Sjekk hvilket nivå ' + result.title + ' har i DBH  <i class="external icon"></i></a>'
          +        '</div>'
          +      '</div>'
          +      '<div class="inline required field">'
          +        '<div class="ui selection dropdown">'
          +        '<input type="hidden" name="dbh_level">'
          +        '<i class="dropdown icon"></i>'
          +        '<div class="default text">Sett DBH nivå</div>'
          +        '<div class="menu">'
          +          '<div class="item" data-value="1">1</div>'
          +          '<div class="item" data-value="2">2</div>'
          +        '</div>'
          +      '</div>'
          +    '</div>'
          +    '<div class="required huge field">'
          +      '<div class="ui huge checkbox">'
          +        '<input type="checkbox" name="DBH_statement">'
          +        '<label>Jeg bekrefter at jeg har sjekket <a href="http://nsd.no">DBH</a>?</label>'
          +      '</div>'
          +    '</div>'
          +    '<div class="ui horizontal divider">Gå til neste steg</div>'  
        ;*/

        html += ''
          +    '<a class="ui primary huge submit button" href="' + skjemaker 
          +      '&element_80=1' // ARTIKKEL
          +      '&element_6=' + result.title // JOURNAL
        ;
        if(result.publisher !== undefined) {
          html += '&element_18=' + result.publisher; // UTGIVER
        }
        if(result.title_url !== undefined) {
          html += '&element_22=' + result.title_url; // UTGIVER/JOURNAL URL
        }
        if(result.eissn !== undefined) {
          html += '&element_71=' + result.eissn; // UTGIVER/JOURNAL URL
        }
        if(result.eissn !== undefined) {
          html += '&element_102=' + result.doaj; // BOOLEAN REGISTRERIT I DOAJ
        }
       if(result.license !== undefined) {
          html += '&element_109=' + result.license; // DOAJ LISENS
        }
        if(result.doaj_price !== undefined) {
          html += '&element_111=' + result.doaj_price; // DOAJ ESTIMERT PRIS
        }
        if(result.currency !== undefined) {
          html += '&element_108=' + result.currency; // DOAJ VALUTA
        }
        if(result.id !== undefined) {
          html += '&element_107=' + result.id; // DOAJ ID PÅ TIDSSKRIFT
        }
        
        html += ''    
          +      '">Proceed to application form</a>'
        ;

        html += '</div>';

        $('.journal-choice').html(html);
        return 'hide results';

      }
  })
;


$('.ui.dropdown')
  .dropdown()
;

$('.ui.form')
  .form({
    fields: {
      gender   : 'minLength[1]',
      DBH_statement    : 'checked'
    }
  })

  $('.ui.accordion')
  .accordion()
;
;