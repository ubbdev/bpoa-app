# BPOA APP

**SISTE**

Vi kan sende verdier til Skjemaker. Foreslår vi lager et js-script som sjekker DAOJ apien og sender det vi trenger til Skjemaker.

* Tidsskriftets navn
* Tidsskriftets URL
* Utgiver
* Utgivers URL
* Gjennomsnitts APC

Eksempel: 

```
https://skjemaker.app.uib.no/view.php?id=xxxxxx&element_5=Hvordan%20lage%20dokumentasjon&element_6=PLoS%20One&element_12=2&element_27=3&element_55_1=1
```

Skjemaker kan sende til en API: https://www.machform.com/blog-webhook-api-integration/

Krav:

* API kall med javascript, lage hele app i js? Node?
* Document store og/eller SPARQL endpoint
* Elasticsearch index
* Visualisering av data

## Dagens datamodell

System:

* Entry # 
* Date Created  
* Date Updated  
* IP Address  

Godkjennelse av krav:

* I am the corresponding author 
* I am affiliated to the University of Bergen as an employee, PhD-candidate or master student 
* The work is accepted for publication, but not yet published.  
* The publication is peer-reviewed  
* The University of Bergen will be credited on the publications statement of my institutional affiliation and the publication will be uploaded in CRIStin as soon as it is published. 

Autentisering:

* My name - First 
* My name - Last  
* Email 
* Position  
* Master students need to upload a written approval from their supervisor or project leader.  
* Faculty/centre  
* Institute/unit 

Artikkelinformasjon:

* Title of work 
* Name of journal 
* Name of publisher 
* Publisher or journal website  

APC:

* Estimated application amount  
* Currency  
* Does the research the article is based upon have external funding?  
* Additional information or comments  

UB saksbehandling:

* Saksbehandler 
* DOAJ  
* DBH 
* Behandlet dato  
* Beløp oppgitt på utgivers nettside  
* Innvilget 
* Årsak til avslag 
* Estimert beløp i NOK inkludert MVA  
* Type Open Access  
* Fakturabeløp i NOK  
* Fakturabeløp i NOK inkludert MVA  
* Forfallsdato  
* UiB angitt som institusjonstilhørighet  
* Åpen lisens 
* Lenke til BORA  
* Andre kommentarer:  
* Ferdigstilt

## Neste versjon

1. Autentisering
    * UiB LDAP?
    * Dataporten?
2. Godkjenning av vilkår
    * I am the corresponding author 
    * I am affiliated to the University of Bergen as an employee, PhD-candidate or master student
    * The work is accepted for publication, but not yet published.  
    * The publication is peer-reviewed  
    * The University of Bergen will be credited on the publications statement of my institutional affiliation and the publication will be uploaded in CRIStin as soon as it is published. 
    * Master students need to upload a written approval from their supervisor or project leader. 
3. Personopplysninger
    * My name - First **Hentes fra LDAP respons**
    * My name - Last **Hentes fra LDAP respons**
    * Email **Hentes fra LDAP respons**
    * Position **Hentes fra LDAP respons**
    * ~~Master students need to upload a written approval from their supervisor or project leader.~~ __Bør gjøres først slik at studenter hindres å gå inn i skjemaet__
    * Faculty/centre **Hentes fra LDAP respons, men kan overstyres?**
    * Institute/unit **Hentes fra LDAP respons, men kan overstyres?**

4. Artikkelinformasjon:
    * Name of journal **Input kaller DAOJ API**
    * Title of work 
    * Name of publisher **Hentes fra DOAJ respons**
    * Publisher or journal website **Hentes fra DOAJ respons**
5. APC:
    * Estimated application amount **Hentes fra DOAJ respons, men kan overstyres?**
    * Currency **Hentes fra DOAJ respons, men kan overstyres?**
    * Populere felt for alle valuta på dette steget?
    * Does the research the article is based upon have external funding?  
    * Additional information or comments  

### Saksbehandling
Egen klasse/type i store? Koblet til søknad? Flytte felt til Artikkelinformasjon (DBH sjekk etter DOAJ kall, Type Open Access, Åpen lisens, Lenke til BORA)?

* Saksbehandler 
* DOAJ  
* DBH 
* Behandlet dato  
* Beløp oppgitt på utgivers nettside  
* Innvilget 
* Årsak til avslag 
* Estimert beløp i NOK inkludert MVA  
* Type Open Access  
* Fakturabeløp i NOK  
* Fakturabeløp i NOK inkludert MVA  
* Forfallsdato  
* UiB angitt som institusjonstilhørighet  
* Åpen lisens 
* Lenke til BORA  
* Andre kommentarer:  
* Ferdigstilt

## API vi kan benytte

### DOAJ API

[https://doaj.org/api/v1/docs]

#### GET /api/v1/search/journals/{search_query}

Base URL: http://doaj.org/api/v1/

Dette er det man kan forvente seg fra api'en når man søker etter et tidsskrift. Man kan søke i flere felt:

* title - search within the journal's title
* issn - the journal's issn
* publisher - the journal's publisher (not exact match)
* license - the exact licence

```json
{
  "pageSize": 0,
  "timestamp": "2016-08-18T07:14:35.011Z",
  "results": [
    {
      "last_updated": "2016-08-18T07:14:35.011Z",
      "id": "string",
      "bibjson": {
        "author_pays": "string",
        "allows_fulltext_indexing": true,
        "archiving_policy": {
          "policy": [
            "string"
          ],
          "url": "string"
        },
        "author_publishing_rights": {
          "url": "string",
          "publishing_rights": "string"
        },
        "keywords": [
          "string"
        ],
        "apc": {
          "currency": "string",
          "average_price": "string"
        },
        "subject": [
          {
            "code": "string",
            "term": "string",
            "scheme": "string"
          }
        ],
        "article_statistics": {
          "url": "string",
          "statistics": true
        },
        "title": "string",
        "author_pays_url": "string",
        "publication_time": "string",
        "provider": "string",
        "format": [
          "string"
        ],
        "submission_charges": {
          "currency": "string",
          "average_price": "string"
        },
        "apc_url": "string",
        "plagiarism_detection": {
          "detection": true,
          "url": "string"
        },
        "link": [
          {
            "url": "string",
            "type": "string"
          }
        ],
        "oa_start": {
          "volume": "string",
          "number": "string",
          "year": "string"
        },
        "editorial_review": {
          "process": "string",
          "url": "string"
        },
        "author_copyright": {
          "url": "string",
          "copyright": "string"
        },
        "institution": "string",
        "deposit_policy": [
          "string"
        ],
        "license": [
          {
            "title": "string",
            "url": "string",
            "NC": true,
            "ND": true,
            "embedded_example_url": "string",
            "SA": true,
            "type": "string",
            "BY": true
          }
        ],
        "alternative_title": "string",
        "country": "string",
        "publisher": "string",
        "submission_charges_url": "string",
        "persistent_identifier_scheme": [
          "string"
        ],
        "oa_end": {
          "volume": "string",
          "number": "string",
          "year": "string"
        },
        "identifier": [
          {
            "type": "string",
            "id": "string"
          }
        ]
      },
      "created_date": "2016-08-18T07:14:35.014Z"
    }
  ],
  "query": "string",
  "total": 0,
  "page": 0
}
```

### Autentisering

#### ORCID autentisering

* Hvordan autentisere med ORCID: [http://members.orcid.org/api/tutorial-retrieve-orcid-id]
* XML fra ORCID: [http://members.orcid.org/api/record-xml-structure]

#### Dataporten autentisering

[https://docs.dataporten.no/]

[https://docs.dataporten.no/docs/oauth-libraries/] det er flere bibliotek som kan brukes i node, meteor og js.

### Valuta konvertering

Det er mange forskjellig API'er, men denne var helt gratis. Uansett så vil vi ha så få kall at vi ikke må betale.

##### fixer.io

[http://api.fixer.io/2010-01-01?base=USD&symbols=NOK,EUR,USD,AUD]

```json
{
  "base":"USD",
  "date":"2009-12-31",
  "rates": {
    "AUD":1.1112,
    "NOK":5.7615,
    "EUR":0.69416
  }
}
```

### Saksbehandling

#### Redmine Helpdesk API

Det vil ikke være vanskelig å la app'en sende epost til søker. Vi kan også lage en sak i Redmine med API til Helpdesk utvidelsen.

[https://www.redminecrm.com/projects/helpdesk/wiki/REST_API_Helpdesk]

```
Send response to customer
POST /helpdesk/email_note.xml
Parameters:

Parameter	Type	Description
message[issue_id]	integer	ticket id
message[status_id]	integer	set ticket status to status_id after successful sent
message[content]	string	email message content
Create helpdesk ticket
POST /helpdesk/create_ticket.xml
Parameters:

Parameter	Type	Description
ticket[issue][project_id]	integer	Create ticket in project with id
ticket[issue][tracker_id]	string	New ticket tracker
ticket[issue][description]	string	New ticket content
... any other fields from Issues REST API
ticket[contact][first_name]	string	new customer first name
ticket[contact][last_name]	string	new customer last name
ticket[contact][middle_name]	string	new customer middle name
ticket[contact][tag_list]	string	new customer tags
ticket[contact][company]	string	new customer company
ticket[contact][email]	string	new customer email
... any other fields from Contacts REST API
Examples:

POST /helpdesk/create_ticket.xml
<?xml version="1.0"?>
<ticket>
  <issue>
    <project_id>support</project_id>
    <tracker_id>1</tracker_id>
    <subject>New ticket subject</subject>
    <description>New ticket description</description>
    <assigned_to_id>1</assigned_to_id>
  </issue>
  <contact>
    <email>test@example.com</email>
    <first_name>John</first_name>
    <custom_fields type="array">
       <custom_field id="1">
         <value>Test</value>
       </custom_field>
    </custom_fields>
  </contact>
</ticket>
cURL example:

curl -i -H 'Content-Type: application/xml' -X POST --data '<ticket><issue><project_id>helpdesk</project_id><tracker_id>6</tracker_id><subject>New ticket from API</subject><description>Description for ticket created using REST API</description><assigned_to_id>4</assigned_to_id></issue><contact><email>test@example.com</email><first_name>John</first_name><custom_fields type="array"><custom_field id="1"><value>Test</value></custom_field></custom_fields></contact></ticket>' -u admin:admin http://demo.redminecrm.com/helpdesk/create_ticket.xml
</first_name></contact></ticket>" -u admin:admin http://demo.redminecrm.com/helpdesk/create_ticket.xml 
Existed contact will be founded by email address
```

## Bygge egen API for søknad

* MEAN
    * https://devcenter.heroku.com/articles/mean-apps-restful-api#use-angularjs-services-to-make-requests-to-the-api-server
    * https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4
    * https://pixelhandler.com/posts/develop-a-restful-api-using-nodejs-with-express-and-mongoose
    * http://mean.io/#!/
* ANNE
    * http://www.42id.com/articles/anne-stack-angular-js-node-neo4j-and-express/
    * http://blog.modulus.io/learn-how-to-use-neo4j-with-node.js?utm_content=12954986&utm_medium=social&utm_source=twitter
* YMSE
    * http://www.js-data.io/docs/home
    * http://restify.com/